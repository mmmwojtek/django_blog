from django.db import models
from django.utils.timezone import now


class Category(models.Model):
    name = models.CharField(max_length=30, unique=True)
    created = models.DateTimeField(default=now, editable=False)

    def __str__(self) -> str:
        return f"{self.name}"


class Article(models.Model):
    title = models.CharField(max_length=100)
    pub_date = models.DateField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["title"]
