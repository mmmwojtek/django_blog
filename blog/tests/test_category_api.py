import json

import django
import pytest
from django.test.client import Client
from django.urls import reverse

from blog.models import Category

django.setup()
articles_url = reverse("articles-list")
categories_url = reverse("categories-list")

pytestmark = pytest.mark.django_db


# -------------------Test Get Categories------------


def test_zero_categories_should_return_empty_list(client: Client) -> None:
    response = client.get(categories_url)
    assert response.status_code == 200
    assert json.loads(response.content) == []


def test_one_category_exist_should_succeed(client: Client, category: Category) -> None:
    response = client.get(categories_url)
    response_content = json.loads(response.content)[0]
    assert response.status_code == 200
    assert response_content.get("name") == category.name


# -------------------Test Get Category------------


def test_zero_categories_or_wrong_id_should_return_not_found(client: Client) -> None:
    response = client.get(categories_url + "/1")
    assert response.status_code == 404


def test_category_id_one_exists_should_succeed(
    client: Client, category: Category
) -> None:
    response = client.get(categories_url + str(category.id) + "/")
    response_content = json.loads(response.content)
    assert response.status_code == 200
    assert response_content.get("name") == category.name
    assert response_content.get("id") == category.id


# -------------------Test Post Categories------------


def test_create_category_without_arguments_should_fail(client: Client) -> None:
    response = client.post(path=categories_url)
    response_content = json.loads(response.content)
    assert response.status_code == 400
    assert response_content == {"name": ["This field is required."]}


def test_create_existing_category_should_fail(
    client: Client, category: Category
) -> None:
    response = client.post(path=categories_url, data={"name": category.name})
    response_content = json.loads(response.content)
    assert response.status_code == 400
    assert response_content == {"name": ["category with this name already exists."]}


def test_create_category_should_succeed(client: Client) -> None:
    category_name = "test company name"
    response = client.post(path=categories_url, data={"name": category_name})
    response_content = json.loads(response.content)
    assert response.status_code == 201
    assert response_content.get("name") == category_name


def test_create_category_with_too_long_name_should_fail(client: Client) -> None:
    category_name = "test company name test company name test company name"
    response = client.post(path=categories_url, data={"name": category_name})
    response_content = json.loads(response.content)
    assert response.status_code == 400
    assert response_content == {
        "name": ["Ensure this field has no more than 30 characters."]
    }


# -------------------Test Patch Category------------


def test_patch_category_should_succeed(client: Client, category: Category) -> None:
    new_category_name = "Books2"
    response = client.patch(
        categories_url + str(category.id) + "/",
        data={"name": new_category_name},
        content_type="application/json",
    )
    response_content = json.loads(response.content)
    assert response.status_code == 200
    assert response_content.get("name") == new_category_name


# -------------------Test Put Category------------


def test_put_category_should_succeed(client: Client, category: Category) -> None:
    new_category_name = "Books2"
    response = client.put(
        categories_url + str(category.id) + "/",
        data={"name": new_category_name},
        content_type="application/json",
    )
    response_content = json.loads(response.content)
    assert response.status_code == 200
    assert response_content.get("name") == new_category_name


# -------------------Test Delete Category------------


def test_delete_category_should_succeed(client: Client, category: Category) -> None:
    response = client.delete(categories_url + str(category.id) + "/")
    assert response.status_code == 204
