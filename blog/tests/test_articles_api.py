import json

import django
import pytest
from django.test.client import Client
from django.urls import reverse

from blog.models import Article, Category

django.setup()
articles_url = reverse("articles-list")

pytestmark = pytest.mark.django_db


# -------------------Test Get Articles------------


def test_zero_articles_should_return_empty_list(client: Client) -> None:
    response = client.get(articles_url)
    assert response.status_code == 200
    assert json.loads(response.content) == []


def test_one_article_exists_should_succeed(client: Client, article: Article) -> None:
    response = client.get(articles_url)
    response_content = json.loads(response.content)[0]
    assert response.status_code == 200
    assert response_content.get("title") == article.title
    assert response_content.get("pub_date") == article.pub_date
    assert response_content.get("category_name") == article.category.name


# -------------------Test Get Article------------


def test_zero_articles_or_wrong_id_should_return_not_found(client: Client) -> None:
    response = client.get(articles_url + "/1")
    assert response.status_code == 404


def test_category_id_one_exists_should_succeed(client, article: Article) -> None:
    response = client.get(articles_url + str(article.id) + "/")
    response_content = json.loads(response.content)
    assert response.status_code == 200
    assert response_content.get("title") == article.title
    assert response_content.get("pub_date") == article.pub_date
    assert response_content.get("category_name") == article.category.name


# -------------------Test Post Article------------


def test_create_article_without_arguments_should_fail(client: Client) -> None:
    response = client.post(path=articles_url)
    assert response.status_code == 400
    assert "This field is required." in str(response.content)


def test_create_article_should_succeed(client: Client, category: Category) -> None:
    article_title = "Witcher"
    article_pub_date = "2021-12-12"
    response = client.post(
        path=articles_url,
        data={
            "title": article_title,
            "pub_date": article_pub_date,
            "category": category.id,
        },
    )
    response_content = json.loads(response.content)
    assert response.status_code == 201
    assert response_content.get("title") == article_title
    assert response_content.get("pub_date") == article_pub_date
    assert response_content.get("category_name") == category.name


def test_create_article_with_too_long_title_should_fail(
    client: Client, category: Category
) -> None:
    article_title = (
        "WitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcher"
        "WitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcher"
        "WitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcher"
        "WitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcher"
        "WitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcher"
        "WitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcher"
        "WitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcherWitcher"
    )
    article_pub_date = "2021-12-12"
    response = client.post(
        path=articles_url,
        data={
            "title": article_title,
            "pub_date": article_pub_date,
            "category": category.id,
        },
    )
    response_content = json.loads(response.content)
    assert response.status_code == 400
    assert response_content == {
        "title": ["Ensure this field has no more than 100 characters."]
    }


def test_create_article_with_wrong_date_format_should_fail(
    client: Client, category: Category
) -> None:
    article_title = "Witcher"
    article_pub_date = "1-12-12"
    response = client.post(
        path=articles_url,
        data={
            "title": article_title,
            "pub_date": article_pub_date,
            "category": category.id,
        },
    )
    response_content = json.loads(response.content)
    assert response.status_code == 400
    assert response_content == {
        "pub_date": [
            "Date has wrong format. Use one of these formats instead: YYYY-MM-DD."
        ]
    }


def test_create_article_with_wrong_category_should_fail(client: Client) -> None:
    article_title = "Witcher"
    article_pub_date = "2012-12-12"
    response = client.post(
        path=articles_url,
        data={"title": article_title, "pub_date": article_pub_date, "category": "xx"},
    )
    response_content = json.loads(response.content)

    assert response.status_code == 400
    assert response_content == {
        "category": ["Incorrect type. Expected pk value, received str."]
    }


# -------------------Test Patch Article------------


def test_patch_article_should_succeed(client: Client, article: Article) -> None:
    new_article_title = "Witcher2"
    response = client.patch(
        articles_url + str(article.id) + "/",
        data={"title": new_article_title},
        content_type="application/json",
    )
    response_content = json.loads(response.content)
    assert response.status_code == 200
    assert response_content.get("title") == new_article_title


# -------------------Test Put Article------------


def test_put_article_should_succeed(
    client: Client, article: Article, category: Category
) -> None:
    new_article_title = "Witcher2"
    new_article_pub_date = "2012-12-12"
    response = client.put(
        articles_url + str(article.id) + "/",
        data={
            "title": new_article_title,
            "pub_date": new_article_pub_date,
            "category": category.id,
        },
        content_type="application/json",
    )
    response_content = json.loads(response.content)
    assert response.status_code == 200
    assert response_content.get("title") == new_article_title
    assert response_content.get("pub_date") == new_article_pub_date


# -------------------Test Delete Article------------


def test_delete_article_should_succeed(client: Client, article: Article) -> None:
    response = client.delete(articles_url + str(article.id) + "/")
    assert response.status_code == 204
