from rest_framework import routers

from blog.views import ArticleViewSet, CategoryViewSet

blog_router = routers.DefaultRouter()
blog_router.register("articles", viewset=ArticleViewSet, basename="articles")
blog_router.register("categories", viewset=CategoryViewSet, basename="categories")
