from rest_framework import serializers

from blog.models import Article, Category


class ArticleSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source="category.name", read_only=True)

    class Meta:
        model = Article
        fields = ["id", "title", "pub_date", "category", "category_name"]


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "name", "created"]
