from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet

from blog.models import Article, Category
from blog.serializers import ArticleSerializer, CategorySerializer


class ArticleViewSet(ModelViewSet):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all().order_by("-pub_date")
    pagination_class = PageNumberPagination


class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    pagination_class = PageNumberPagination
