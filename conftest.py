import pytest

from blog.models import Article, Category


@pytest.fixture()
def category() -> Category:
    category = Category.objects.create(name="Thriller")
    yield category
    category.delete()


@pytest.fixture()
def article() -> Article:
    article_category = Category.objects.create(name="Fantasy")
    article = Article.objects.create(
        title="Witcher",
        pub_date="2021-12-12",
        category=article_category,
    )
    yield article
    article.delete()
    article_category.delete()
