# django_blog



## Requirements

- Docker (20.10+)
- docker-compose (1.29+)

## Run project

```
docker-compose up --build -d 
```

## Useful links in project

- [ ] [Swagger](http://127.0.0.1:4000/swagger/)
- [ ] [Redoc](http://127.0.0.1:4000/redoc/)
- [ ] [Admin panel](http://127.0.0.1:4000/admin/)